package suitLmsAlchemy;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity4 {
/*Verify the title of the first info box
	Goal: Read the title of the first info box on the website and verify the text
	Open a browser.
	Navigate to �https://alchemy.hguy.co/lms�. 
	Get the title of the first info box.
	Make sure it matches �Actionable Training� exactly.
	If it matches, close the browser.*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         @Test
		
		    public void testCase1() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			//This test case will pass
	        js.executeScript("window.scrollBy(0,500)");
	    	//WebElement element = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[3]/div[2]/div[2]/div[2]/div/div/div/div/div[1]/h2"));
	    	WebElement element = driver.findElement(By.cssSelector("#uagb-infobox-9859b65d-80ad-400c-94f9-5de5bc46cc50 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1)"));
	    	System.out.println("Most popular course is: " +element.getText());
	    	String actualHeading= "Our Most Popular Courses";
	    	String element1 = element.getText();
	    	Assert.assertEquals(element1, actualHeading);
	    	System.out.println("Most popular course is: " +actualHeading);
	        Thread.sleep(10000);
	        driver.close();
	    }

	
		}  