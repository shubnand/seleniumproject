package suitLmsAlchemy;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity1 {
	    WebDriver driver;
	    Actions builder;
	    
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
	    
	    @Test
	    public void testCase1() throws InterruptedException {
	        //This test case will pass
	    	 String title = driver.getTitle();
		        System.out.println("Title is: " + title);
	    	String actualTitle= "Alchemy LMS � An LMS Application";
	    	
	       
	        Assert.assertEquals(driver.getTitle(), actualTitle);
	        Thread.sleep(10000);
	        driver.close();
	    }  
	   
}
