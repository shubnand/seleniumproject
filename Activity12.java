package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
/* Use XPath to find an element on the page
Goal: Using XPath to find elements on the page and complete a lesson.
Open the browser to the All Courses page of Alchemy LMS site.
Find a course to open using XPath notations.
Find a lesson in that course and open it using XPath notations.
Find the Mark Complete button on the page using XPath and click it.
Check progress of course by finding the progress bar on the page using XPath
Close the browser.*/


public class Activity12 {
	 WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
			// TODO Auto-generated method stub
		
		// Complete an entire course
		//Goal: Navigate to a particular course and complete all lessons and topics in it.
	    public void testCase1() throws InterruptedException {
       	 JavascriptExecutor js = (JavascriptExecutor) driver;
		// TODO Auto-generated method stub
		
		 //Use XPath to find an element on the page
		 //Goal: Using XPath to find elements on the page and complete a lesson.
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement allCourses = driver.findElement(By.xpath("//a[@href = 'https://alchemy.hguy.co/lms/all-courses/']"));
		allCourses.click();
		WebElement courses = driver.findElement(By.xpath("//a[@class = 'btn btn-primary']"));
		courses.click();
		WebElement login = driver.findElement(By.cssSelector(".ld-button"));
		login.click();
		WebElement userName = driver.findElement(By.xpath("//input[@id = 'user_login']"));
		userName.sendKeys("root");
		WebElement password = driver.findElement(By.xpath("//input[@id = 'user_pass']"));
		password.sendKeys("pa$$w0rd");
		WebElement submit = driver.findElement(By.xpath("//input[@id = 'wp-submit']"));
		submit.click();
		WebElement lesson = driver.findElement(By.xpath("//a [@class = 'ld-item-name ld-primary-color-hover']"));
		lesson.click();
		String heading = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Heading is: " + heading);
		WebElement lessonContent = driver.findElement(By.xpath("//a [@href = 'https://alchemy.hguy.co/lms/topic/this-is-the-first-topic/']"));
		lessonContent.click();
		String headingContent = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Lesson Content Heading is: " + headingContent);
		String courseCompletion = driver.findElement(By.xpath("//div [@class = 'ld-progress-percentage ld-secondary-color']")).getText();
		System.out.println("Course Completion Percentage is: " + courseCompletion);
		driver.close();
		
	}
}

