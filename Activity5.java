package suitLmsAlchemy;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class Activity5 {
/*Verify the title of the first info box
	Goal: Read the title of the first info box on the website and verify the text
	Open a browser.
	Navigate to �https://alchemy.hguy.co/lms�. 
	Get the title of the first info box.
	Make sure it matches �Actionable Training� exactly.
	If it matches, close the browser.*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         public void testCase1() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			//This test case will pass
	        	//WebElement element = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[3]/div[2]/div[2]/div[2]/div/div/div/div/div[1]/h2"));
	    	WebElement element = driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a"));
	    	 Thread.sleep(10000);
	    	element.click();
	    	System.out.println("My Account click: " +element.getText());
	    	WebElement element1 = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[1]/div[2]/div[2]/div[2]/div/div/div/div/div[1]/h1"));
	    	String actualHeading= "My Account";
	    	String element2 = element1.getText();
	    	Assert.assertEquals(element2, actualHeading);
	    	System.out.println("Actual heading is: " +actualHeading);
	        Thread.sleep(10000);
	        driver.close();
	    }

		         	
		}  
