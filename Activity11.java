package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
/*

 Complete an entire course
Goal: Navigate to a particular course and complete all lessons and topics in it.
Open the browser to the All Courses page of Alchemy LMS site.
Select any course and open it.
Click on a lesson in the course. Verify the title of the course.
Open a topic in that lesson. Mark it as complete.
Perform the above steps for all lessons and topics in the course.
Verify the course is complete by checking the value of the progress bar.
Close the browser.
*/

public class Activity11 {
	 WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
			// TODO Auto-generated method stub
		
		// Complete an entire course
		//Goal: Navigate to a particular course and complete all lessons and topics in it.
	    public void testCase1() throws InterruptedException {
       	 JavascriptExecutor js = (JavascriptExecutor) driver;
		
		WebElement allCourses = driver.findElement(By.cssSelector("#menu-item-1508 > a:nth-child(1)"));
		allCourses.click();
		WebElement courses = driver.findElement(By.cssSelector("#post-69 > div:nth-child(3) > p:nth-child(3) > a:nth-child(1)"));
		courses.click();
		WebElement login = driver.findElement(By.cssSelector(".ld-button"));
		login.click();
		WebElement userName = driver.findElement(By.id("user_login"));
		userName.sendKeys("root");
		WebElement password = driver.findElement(By.id("user_pass"));
		password.sendKeys("pa$$w0rd");
		WebElement submit = driver.findElement(By.id("wp-submit"));
		submit.click();
		WebElement title = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)"));
		title.click();
		WebElement lesson = driver.findElement(By.cssSelector("#ld-table-list-item-175 > a:nth-child(1) > span:nth-child(2)"));
		lesson.click();
		String heading = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		System.out.println("Heading is: " + heading);
		WebElement lesson1 = driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1) > span:nth-child(1)"));
		lesson1.click();
		String heading1 = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		System.out.println("Heading is: " + heading1);
		WebElement lesson2 = driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1) > span:nth-child(1)"));
		lesson2.click();
		String heading2 = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		System.out.println("Heading is: " + heading2);
		String courseCompletion = driver.findElement(By.cssSelector("div.ld-progress-stats:nth-child(1) > div:nth-child(1)")).getText();
		System.out.println("Course Completion Percentage is: " + courseCompletion);
		driver.close();

	}

}

