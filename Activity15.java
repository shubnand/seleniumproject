package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity15 {
	 WebDriver driver;

		 @BeforeClass
	    public void setUp() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	        Reporter.log("Starting Test |");
	        //Open browser
	        driver.navigate().to("https://alchemy.hguy.co/lms");
	        Reporter.log("Opened Browser |");
	        //Print title of page
	        Reporter.log("Page title is " + driver.getTitle() + " |");
	    }
				
	    @Test
	    public void testCase() {
	    	Reporter.log("Select All Courses |");
	    	WebElement allCourses = driver.findElement(By.cssSelector("#menu-item-1508 > a:nth-child(1)"));
			allCourses.click();
			Reporter.log("Selected All Courses |");
			Reporter.log("Find Courses |");
			WebElement courses = driver.findElement(By.cssSelector("#post-69 > div:nth-child(3) > p:nth-child(3) > a:nth-child(1)"));
			courses.click();
			Reporter.log("Login |");
			WebElement login = driver.findElement(By.cssSelector(".ld-button"));
			login.click();
			WebElement userName = driver.findElement(By.id("user_login"));
			userName.sendKeys("root");
			WebElement password = driver.findElement(By.id("user_pass"));
			password.sendKeys("pa$$w0rd");
			WebElement submit = driver.findElement(By.id("wp-submit"));
			submit.click();
			Reporter.log("Login completed |");
			Reporter.log("Search Lesson |");
			WebElement title = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)"));
			title.click();
			WebElement lesson = driver.findElement(By.cssSelector("#ld-table-list-item-175 > a:nth-child(1) > span:nth-child(2)"));
			lesson.click();
			String heading = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
			System.out.println("Heading is: " + heading);
			WebElement lesson1 = driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1) > span:nth-child(1)"));
			lesson1.click();
			String heading1 = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
			System.out.println("Heading is: " + heading1);
			WebElement lesson2 = driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1) > span:nth-child(1)"));
			lesson2.click();
			String heading2 = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
			System.out.println("Heading is: " + heading2);
			Reporter.log("Search Lesson completed |");
			Reporter.log("Find Course Completion |");
			String courseCompletion = driver.findElement(By.cssSelector("div.ld-progress-stats:nth-child(1) > div:nth-child(1)")).getText();
			System.out.println("Course Completion Percentage is: " + courseCompletion);
			Reporter.log("Found Course Completion |");
	    }
		
		
	    @AfterClass
	    public void tearDown() {
	        Reporter.log("Ending Test |");
	        //Close the driver
	        driver.close();
	    }

}
