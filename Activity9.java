package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class Activity9 {
/* Navigate to a particular lesson and complete it.
Open a browser.
Navigate to �https://alchemy.hguy.co/lms�. 
Find the navigation bar.
Select the menu item that says �All Courses� and click it.
Select any course and open it.
Click on a lesson in the course. Verify the title of the course.
Click the Mark Complete button on the page (if available).
Close the browser.

*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         public void testCase1() throws InterruptedException {
        	 JavascriptExecutor js = (JavascriptExecutor) driver;
        	 js.executeScript("window.scrollBy(0,500)");
						//This test case will pass
	    	Thread.sleep(10000);
	    	// List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"ld_course_list\"]"));
	    	WebElement myAccount= driver.findElement(By.linkText("My Account"));
	 			
	    	myAccount.click(); 
	    	WebElement login = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
	    login.click();
	    	WebElement userName = driver.findElement(By.id("user_login"));
	 	    userName.sendKeys("root");
	 	    WebElement userPass = driver.findElement(By.id("user_pass"));
	 	    userPass.sendKeys("pa$$w0rd");
	 	    WebElement subMit = driver.findElement(By.id("wp-submit"));
	 	  subMit.click();
	 	  	 	  System.out.println("logged in successgully ");

	 		        WebElement element1= driver.findElement(By.linkText("All Courses"));
			       	 			
	 		    	element1.click();
	 		    	
	    	List<WebElement> allElements = driver.findElements(By.className("ld_course_grid"));
	    	System.out.println("Totalcourse = "  +allElements.get(0).getText());
	    	 //java.util.List<WebElement> allElements = driver.findElements(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[1]"));
	    	 int elementsCount = allElements.size(); 
	    	 System.out.println("Total Number of courses on webpage = "  + elementsCount);
	    	 
	    	 WebElement seemore = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[2]/div/div/div/div[1]/article/div[2]/p[2]/a"));
	    	 
	    seemore.click();
	    WebElement option= driver.findElement(By.className("ld-item-title"));
	    option.click();
	    WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1"));
	    System.out.println("Title is = "  + title.getText());
	       
	    
	        driver.close();
	    }

		         	
		}  
