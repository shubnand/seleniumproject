package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class Activity7 {
/* Count the number of courses
Goal: Navigate to the �All Courses� page and count the number of courses.
Open a browser.
Navigate to �https://alchemy.hguy.co/lms�. 
Find the navigation bar.
Select the menu item that says �All Courses� and click it.
Find all the courses on the page.
Print the number of courses on the page.
*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         public void testCase1() throws InterruptedException {
        	 JavascriptExecutor js = (JavascriptExecutor) driver;
        	 js.executeScript("window.scrollBy(0,500)");
						//This test case will pass
	        WebElement element1= driver.findElement(By.linkText("All Courses"));
	        		 			
	    	element1.click();
	    	Thread.sleep(10000);
	    	// List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"ld_course_list\"]"));
	    	 
	    	List<WebElement> allElements = driver.findElements(By.className("ld_course_grid"));
	    	System.out.println("Totalcourse = "  +allElements.get(0).getText());
	    	 //java.util.List<WebElement> allElements = driver.findElements(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[1]"));
	    	 int elementsCount = allElements.size(); 
	    	 System.out.println("Total Number of courses on webpage = "  + elementsCount);
	    
	        driver.close();
	    }

		         	
		}  
