package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
/*

 Complete a topic in a lesson
Goal: Navigate to a particular lesson and complete a topic in it.
Open a browser.
Navigate to �https://alchemy.hguy.co/lms�. 
Find the navigation bar.
Select the menu item that says �All Courses� and click it.
Select any course and open it.
Click on a lesson in the course. Verify the title of the course.
Open a topic in that lesson. Mark it as complete.
Mark all the topics in the lesson as complete (if available).
Close the browser.*/


public class Activity10 {
	 WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
			// TODO Auto-generated method stub
		
		// Complete an entire course
		//Goal: Navigate to a particular course and complete all lessons and topics in it.
	    public void testCase1() throws InterruptedException {
       	 JavascriptExecutor js = (JavascriptExecutor) driver;

        WebElement allCourses = driver.findElement(By.cssSelector("#menu-item-1508 > a:nth-child(1)"));
		allCourses.click();
		WebElement courses = driver.findElement(By.cssSelector("#post-69 > div:nth-child(3) > p:nth-child(3) > a:nth-child(1)"));
		courses.click();
		WebElement login = driver.findElement(By.cssSelector(".ld-button"));
		login.click();
		WebElement userName = driver.findElement(By.id("user_login"));
		userName.sendKeys("root");
		WebElement password = driver.findElement(By.id("user_pass"));
		password.sendKeys("pa$$w0rd");
		WebElement submit = driver.findElement(By.id("wp-submit"));
		submit.click();
		WebElement title = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)"));
		title.click();
		WebElement lesson = driver.findElement(By.cssSelector("#ld-table-list-item-175 > a:nth-child(1) > span:nth-child(2)"));
		lesson.click();
		String heading = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		System.out.println("Heading is: " + heading);
		driver.close();

	}

}
