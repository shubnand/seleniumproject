package suitLmsAlchemy;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class Activity100 {
/*Goal: Navigate to a particular lesson and complete a topic in it.
Open a browser.
Navigate to �https://alchemy.hguy.co/lms�. 
Find the navigation bar.
Select the menu item that says �All Courses� and click it.
Select any course and open it.
Click on a lesson in the course. Verify the title of the course.
Open a topic in that lesson. Mark it as complete.
Mark all the topics in the lesson as complete (if available).
Close the browser.


*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         public void testCase1() throws InterruptedException {
        	 JavascriptExecutor js = (JavascriptExecutor) driver;
        	 js.executeScript("window.scrollBy(0,500)");
						//This test case will pass
	    	Thread.sleep(10000);
	    	// List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"ld_course_list\"]"));
	    	WebElement myAccount= driver.findElement(By.linkText("My Account"));
	 			
	    	myAccount.click(); 
	    	WebElement login = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
	    login.click();
	    	WebElement userName = driver.findElement(By.id("user_login"));
	 	    userName.sendKeys("root");
	 	    WebElement userPass = driver.findElement(By.id("user_pass"));
	 	    userPass.sendKeys("pa$$w0rd");
	 	    WebElement subMit = driver.findElement(By.id("wp-submit"));
	 	  subMit.click();
	 	  	 	  System.out.println("logged in successgully ");

	 		        WebElement element1= driver.findElement(By.linkText("All Courses"));
			       	 			
	 		    	element1.click();
	 		    	
	    	List<WebElement> allElements = driver.findElements(By.className("ld_course_grid"));
	    	System.out.println("Totalcourse = "  +allElements.get(0).getText());
	    	 //java.util.List<WebElement> allElements = driver.findElements(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[1]"));
	    	 int elementsCount = allElements.size(); 
	    	 System.out.println("Total Number of courses on webpage = "  + elementsCount);
	    	 
	    	 WebElement seemore = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[2]/div/div/div/div[1]/article/div[2]/p[2]/a"));
	    	 
	    seemore.click();
	    WebElement option= driver.findElement(By.className("ld-item-title"));
	    option.click();
	    WebElement title = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1"));
	    System.out.println("Title is = "  + title.getText());
	       
	    js.executeScript("window.scrollBy(0,1000)");
	    WebElement course = driver.findElement(By.id("ld-table-list-item-175"));
	    course.click();
	    Thread.sleep(10000);
	    /*WebElement nextTopic1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/a"));
	    nextTopic1.click();
	    Thread.sleep(10000);
	    js.executeScript("window.scrollBy(0,1000)");
	    WebElement nextTopic2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/a"));
	    nextTopic2.click();
	    Thread.sleep(10000);
	    /*WebElement nextTopic3 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/a"));
	    nextTopic3.click();
	    Thread.sleep(10000);
	    WebElement nextTopic4 = driver.findElement(By.className("ld-content-action"));
	    nextTopic4.click();
	    Thread.sleep(10000);*/
	    js.executeScript("window.scrollBy(0,1000)");

	        //WebElement backtoLesson= driver.findElement(By.linkText("Back to Lesson"));
	        //backtoLesson.click();
	   //* WebElement course1 = driver.findElement(By.id("ld-table-list-item-200"));
	    //course1.click();
	   //* WebElement course2 = driver.findElement(By.id("ld-table-list-item-24182"));
	    //course2.click();
	   // WebElement markComplete = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[4]/div[2]/form/input[4]"));
	   	    //markComplete.click();
	   	// System.out.println("markcompleted = "  +markComplete.getText());	
	     //js.executeScript("window.scrollBy(0,1000)");
	     WebElement backtoCourse2= driver.findElement(By.linkText("Back to Course"));
	     backtoCourse2.click();
	     WebElement nextTopic4 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[4]/div[2]/a"));
		 nextTopic4.click();
		 js.executeScript("window.scrollBy(0,1000)");
		 WebElement markComplete1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[4]/div[2]/form/input[4]"));
	   	 markComplete1.click();
	   	 System.out.println("markcompleted = "  +markComplete1.getText());	
	     
	    
	       //driver.close();
	    }

		         	
		}  
