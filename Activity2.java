package suitLmsAlchemy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity2 {
	    WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	       	                 
	    }
	    
		@Test
		
	    public void testCase1() throws InterruptedException {
	        //This test case will pass
			  //Open the browser
			 driver = new FirefoxDriver();
			driver.get("https://alchemy.hguy.co/lms");
			WebElement element = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[1]/div[2]/section/div[2]/div[2]/div[2]/div/div/div/div/div[1]/h1"));
	     	System.out.println ("heading is: " +element.getText());
	     	
	    	String actualHeading= "Learn from Industry Experts";
	    	
	    	String element1 = element.getText();
	    	Assert.assertEquals(element1, actualHeading);
	        Thread.sleep(10000);
	        driver.close();
	    }

	
		}  
	   

