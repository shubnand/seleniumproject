package suitLmsAlchemy;
import java.awt.Dimension;
import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class Activity8 {
/* Contact the admin
Goal: Navigate to the �Contact Us� page and complete the form.
Open a browser.
Navigate to �https://alchemy.hguy.co/lms�. 
Find the navigation bar.
Select the menu item that says �Contact� and click it.
Find the contact form fields (Full Name, email, etc.)
Fill in the information and leave a message.
Click submit.
Read and print the message displayed after submission.
Close the browser.
*/

        WebDriver driver;
	    Actions builder;
		
	    @BeforeClass
	    public void beforeClass() {
	        //Create a new instance of the Firefox driver
	        driver = new FirefoxDriver();
	            
	        //Open the browser
	        driver.get("https://alchemy.hguy.co/lms");
	    }
		
         public void testCase1() throws InterruptedException {
        	 
						//This test case will pass
	        WebElement element1= driver.findElement(By.linkText("Contact"));
	        		 			
	    	element1.click();
	    	Thread.sleep(10000);
	    	JavascriptExecutor js = (JavascriptExecutor) driver;
       	    js.executeScript("window.scrollBy(0,1000)");
	    	//java.util.List<WebElement> allElements = driver.findElements(By.xpath("//*[@id=\"ld_course_list\"]"));
       	 //WebElement fname = driver.findElement(By.xpath("//*[@class=\"wpforms-field-label\"]"));
       WebElement fname = driver.findElement(By.xpath("//*[@id=\"wpforms-8-field_0-container\"]"));
     	System.out.println("Full name  = "  + fname.getText());
     	fname.sendKeys("FullName");
     	WebElement email = driver.findElement(By.xpath("//*[@class=\"wpforms-field wpforms-field-email\"]"));
     	System.out.println("email  = "  + email.getText());
     	email.sendKeys("abcd@gmail.com");
     	WebElement subject = driver.findElement(By.id("wpforms-8-field_3-container"));
     	System.out.println("Subject  = "  + subject.getText());	
     	subject.sendKeys("this is for testing");
     	WebElement comment = driver.findElement(By.id("wpforms-8-field_2-container"));
     	System.out.println("Subject  = "  + comment.getText());	
     	comment.sendKeys("entering text in comments box");
     	
    	//WebElement comment = driver.findElement(By.xpath("//*[@class=\"wpforms-field-large\"]"));
     	//System.out.println("email  = "  + comment.getText());
     	
     	//WebElement element4 = driver.findElement(By.xpath("//*[@class=\"wpforms-field wpforms-field-t\"]"));
     	//System.out.println("email  = "  + element4.getText());	//java.util.List<WebElement> allElements = driver.findElements(By.xpath("//*[@class=\"ld-course-list-items row\"]"));
	    	 //java.util.List<WebElement> allElements = driver.findElements(By.xpath("/html/body/div/div/div/div/main/article/div/section[2]/div[1]"));
	    	// Object elementsCount = allElements.size(); 
	    	// System.out.println("Total Number of courses on webpage = "  + elementsCount);
     	
     	//*[@id="wpforms-8-field_0"]
     	
     	/*driver.findElement(By.id("email")).sendKeys("abcd@gmail.com");							
        driver.findElement(By.name("passwd")).sendKeys("abcdefghlkjl");							
        driver.findElement(By.id("SubmitLogin")).submit();*/				
        System.out.println("form submitted with Send Message");	
	        driver.close();
	    }

		         	
		}  
